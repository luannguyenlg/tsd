﻿using Newtonsoft.Json;

namespace SeleniumTests.DataObject
{
    public class AutomobileData
    {
        public string Path => $"\\Resources\\TestData\\{this.GetType().Name}.json";

        [JsonProperty("vin")] public string VIN { get; set; }
        [JsonProperty("year")] public string Year{ get; set; }
        [JsonProperty("make")] public string Make{ get; set; }
        [JsonProperty("model")] public string Model{ get; set; }
        [JsonProperty("isPrimary")] public bool IsPrimary{ get; set; }

    }
}