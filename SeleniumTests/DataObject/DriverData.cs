﻿using Newtonsoft.Json;
using SeleniumTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.DataObject
{
    public class DriverData
    {
        public string Path => $"\\Resources\\TestData\\{this.GetType().Name}.json";

        [JsonProperty("firstName")] public string FirstName { get; set; }
        [JsonProperty("lastName")] public string LastName { get; set; }
        [JsonProperty("middleName")] public string MiddleName { get; set; }
        [JsonProperty("countryAddress")] public string CountryAddress { get; set; }
        [JsonProperty("phone")] public string Phone { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("addressLine1")] public string AddressLine1 { get; set; }
        [JsonProperty("addressLine2")] public string AddressLine2 { get; set; }
        [JsonProperty("city")] public string City { get; set; }
        [JsonProperty("state")] public string State { get; set; }
        [JsonProperty("zip")] public string ZIP { get; set; }
        [JsonProperty("licenseNumber")] public string LicenseNumber { get; set; }
        [JsonProperty("expirationDate")] public string ExpirationDate { get; set; }
        [JsonProperty("dateOfBirth")] public string DateOfBirth { get; set; }
        [JsonProperty("countryBorn")] public string CountryBorn { get; set; }
        [JsonProperty("stateBorn")] public string StateBorn { get; set; }
        [JsonProperty("companyInsurance")] public string CompanyInsurance { get; set; }
        [JsonProperty("companyPolicyNumber")] public string CompanyPolicyNumber { get; set; }
        [JsonProperty("companyExpirationDate")] public string CompanyExpirationDate { get; set; }

        public DriverData generateNewData()
        {
            string now = DateHelper.GetCurrentDateString();
            this.FirstName = string.Format(this.FirstName, now);
            this.Email = string.Format(this.Email, now);
            this.LicenseNumber = string.Format(this.LicenseNumber, now);
            return this;
        }

        public string getFullName()
        {
            return this.FirstName + ' ' + this.LastName;
        }
    }
}
