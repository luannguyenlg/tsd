﻿using Newtonsoft.Json;
using SeleniumTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.DataObject
{
    public class RepairOrderData
    {
        public string Path => $"\\Resources\\TestData\\{this.GetType().Name}.json";

        [JsonProperty("serviceAdvisor")] public string ServiceAdvisor { get; set; }
        [JsonProperty("businessSource")] public string BusinessSource { get; set; }
        [JsonProperty("returnDate")] public string ReturnDate { get; set; }
        [JsonProperty("rateCode")] public string RateCode { get; set; }
        [JsonProperty("modelGroup")] public string ModelGroup { get; set; }
        [JsonProperty("notes")] public string Notes { get; set; }
        [JsonProperty("unitNumber")] public string UnitNumber { get; set; }

        public RepairOrderData generateNewData(string unitNumber = null)
        {
            this.ReturnDate = DateHelper.GetNextNDateString("MMddyyyy", addDays: 5);
            if (unitNumber != null)
                this.UnitNumber = unitNumber;
            return this;
        }
    }
}
