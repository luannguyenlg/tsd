﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.Helpers
{
    public static class StringHelper
    {
        public static string randomNumber(int from = 1, int to = 9999)
        {
            Random random = new Random();
            return random.Next(from, to).ToString();
        }
    }
}
