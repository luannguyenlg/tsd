﻿using SeleniumCSharp.Core.DriverWrapper;
using SeleniumCSharp.Core.ElementWrapper;
using SeleniumTests.DataObject;
using SeleniumTests.PageObject.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Agreement
{
    class CloseAgreementPage : BasePage
    {
        #region Elements
        private Button _btnClose = new Button("//button[text()='Close']");

        private TextBox _txtMilesIn = new TextBox("id=tsd-miles-in");
        private TextBox _txtMilesOut = new TextBox("id=tsd-miles-out");
        private TextBox _txtReturnDate = new TextBox("//tsd-date[contains(@value.bind,'endDateOnly ')]//input");

        #endregion

        public CloseAgreementPage()
        {
            RequiredElement = _txtMilesIn;
        }
        #region Business Methods

        public void enterMilesIn(string value)
        {
            _txtMilesIn.SendKeys(value);
        }

        public string getValueMilesOut()
        {
            return _txtMilesOut.GetValue();
        }

        public string getValueReturnDate()
        {
            return _txtReturnDate.GetValue();
        }

        public void clickButtonClose()
        {
            _btnClose.ScrollToView();
            _btnClose.Click();
            waitForLoadingCompleted(30);
        }
        #endregion
    }
}
