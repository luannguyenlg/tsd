﻿using SeleniumCSharp.Core.DriverWrapper;
using SeleniumCSharp.Core.ElementWrapper;
using SeleniumTests.DataObject;
using SeleniumTests.PageObject.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Agreement
{
    class NewAgreementPage : BasePage
    {
        #region Elements
        private Button _btnAddDriver = new Button("//p//button[text()='Add Driver']");
        private Button _btnSaveAutomobile = new Button("//button[text()='Save Automobile']");
        private Button _btnOpenAndSign = new Button("//button[text()='Open and Sign']");
        private Button _btnOpenAndPrint = new Button("//button[text()='Open and Print']");
        private Button _btnChangeUnit = new Button("//button[text()='Change Unit']");
        private Button _btnAddCustomerAutomobile = new Button("//button[text()='Add Customer Automobile']");
        private Button _btnAddDriverDialog = new Button("//ux-dialog//button[text()='Add Driver']");
        private Button _btnAddFees = new Button("//tsd-button[@click.delegate='addFee()']//button");


        private TextBox _txtFirstname = new TextBox("id=firstName");
        private TextBox _txtExpirationDate = new TextBox("id=license-expiration");
        private TextBox _txtDateOfBirth = new TextBox("id=dateOfBirth");
        private TextBox _txtLicenseNumber = new TextBox("//input[@data-qa='driver-license-number']");
        private TextBox _txtInsuranceCompany = new TextBox("id=insurance-company");
        private TextBox _txtPolicyNumber = new TextBox("id=policy-number");
        private TextBox _txtInsuranceExpirationDate = new TextBox("//input[@data-qa='insurance-expiration-date']");
        
        private TextBox _txtFirstNameDialog = new TextBox("//ux-dialog//tsd-form-element[contains(@value.bind,'firstName')]//input");
        private TextBox _txtLastNameDialog = new TextBox("//ux-dialog//tsd-form-element[contains(@value.bind,'lastName')]//input");
        private TextBox _txtLicenseNumberDialog = new TextBox("//ux-dialog//input[@data-qa='driver-license-number']");
        private TextBox _txtExpirationDateDialog = new TextBox("//ux-dialog//input[@id='license-expiration']");
        private TextBox _txtDateOfBirthDialog = new TextBox("//ux-dialog//input[@data-qa='dateOfBirth']");
        private ComboBox _cbCountryDialog = new ComboBox("//ux-dialog//select[@data-qa='countries']");
        private ComboBox _cbStateDialog = new ComboBox("//ux-dialog//select[@data-qa='regions']");

        private TextBox _txtCheckOutDate = new TextBox("id=startDatePicker");
        private TextBox _txtReturnDate = new TextBox("id=endDatePicker");
        private TextBox _txtMilesOut= new TextBox("id=tsd-miles-in");
        private TextBox _txtAgreementNotes = new TextBox("//tsd-textarea-element[contains(@value.bind,'model.agreement.notes')]//textarea");

        private TextBox _txtVIN = new TextBox("//tsd-form-element[contains(@value.bind,'vin')]//input");
        private TextBox _txtYear = new TextBox("//tsd-form-element[contains(@value.bind,'autoYear')]//input");
        private TextBox _txtMake = new TextBox("//tsd-form-element[contains(@value.bind,'autoMake')]//input");
        private TextBox _txtModel = new TextBox("//tsd-form-element[contains(@value.bind,'autoModel')]//input");
        private CheckBox _ckIsPrimary = new CheckBox("//tsd-checkbox-switch[contains(@checked.bind,'isPrimary')]//input");

        private CheckBox _ckFeeRowSelect = new CheckBox("//tbody//tr[contains(normalize-space(),'{0}')]//input[@type='checkbox']");
        private CheckBox _ckOpenWithoutSignature = new CheckBox("//tsd-checkbox-element[@elementid='open-without-signature']//input");

        private ComboBox _cbCountryBorn = new ComboBox("id=countries");
        private ComboBox _cbStateBorn = new ComboBox("id=regions");
        private ComboBox _cbBusinessSource = new ComboBox("id=repair-order-type");
        private ComboBox _cbRateCode = new ComboBox("id=rate-code-selector");
        private ComboBox _cbModelGroup = new ComboBox("id=model-group-selector");

        private BaseElement _eleRateCharges = new BaseElement("//div[@class='tsd-rate-selected']");
        private BaseElement _eleUnitRow = new BaseElement("//section[@class='tsd-automobile-availability']//tr[.//td[contains(text(),'{0}')] and .//td[not(contains(normalize-space(),'MOTORQ5'))]][{1}]");
       
        #endregion

        public NewAgreementPage()
        {
            RequiredElement = _txtFirstname;
        }
        #region Business Methods
        public void fillAgreement(DriverData driverData)
        {
            _txtFirstname.SendKeys(driverData.FirstName);
            _txtLicenseNumber.SendKeys(driverData.LicenseNumber);
            _txtExpirationDate.SendKeys(driverData.ExpirationDate);
            _txtDateOfBirth.SendKeys(driverData.DateOfBirth);
            _cbCountryBorn.SelectByText(driverData.CountryBorn);
            _cbStateBorn.SelectByText(driverData.StateBorn);
        }
        
        public void fillInsurance(DriverData driverData)
        {
            _txtInsuranceCompany.SendKeys(driverData.CompanyInsurance);
            _txtPolicyNumber.SendKeys(driverData.CompanyPolicyNumber);
            _txtInsuranceExpirationDate.SendKeys(driverData.CompanyExpirationDate);
        }

        public void fillRate(RepairOrderData data)
        {
            _cbRateCode.SelectByText(data.RateCode, true);
            _cbModelGroup.SelectByText(data.ModelGroup, true);
        }

        public void fillRepairOrder(RepairOrderData data)
        {
            _cbBusinessSource.SelectByText(data.BusinessSource);
        }


        public void addDriver(DriverData driver)
        {
            _btnAddDriver.ClickByJs();
            _txtFirstNameDialog.SendKeys(driver.FirstName);
            _txtLastNameDialog.SendKeys(driver.LastName);
            _txtLicenseNumberDialog.SendKeys(driver.LicenseNumber);
            _txtExpirationDateDialog.SendKeys(driver.ExpirationDate);
            _txtDateOfBirthDialog.SendKeys(driver.DateOfBirth);
            _cbCountryDialog.SelectByText(driver.CountryBorn);
            _cbStateDialog.SelectByText(driver.StateBorn);
            _btnAddDriverDialog.Click();
            _btnAddDriverDialog.WaitForInVisible(180);
            waitForLoadingCompleted();
        }

        public void addAutomobile(AutomobileData data)
        {
            _btnAddCustomerAutomobile.ScrollToView();
            _btnAddCustomerAutomobile.Click();
            _txtVIN.SendKeys(data.VIN);
            _txtYear.SendKeys(data.Year);
            _txtMake.SendKeys(data.Make);
            _txtModel.SendKeys(data.Model);
            if(data.IsPrimary)
                _ckIsPrimary.Check();
            _btnSaveAutomobile.Click();
            _btnSaveAutomobile.WaitForInVisible(180);
        }

       public void addFee(string content)
        {
            _btnAddFees.ScrollToView();
            _btnAddFees.Click();
            _ckFeeRowSelect.Format(content);
            _ckFeeRowSelect.ClickByJs();
            saveDialog();
        }

        public void changeMilesOut(string value)
        {
            _txtMilesOut.SendKeys(value);
        }

        public void changeAgreementNotes(string value)
        {
            _txtAgreementNotes.SendKeys(value);
        }

        public string getValueCheckoutDate()
        {
            return _txtCheckOutDate.GetValue();
        }

        public string getTextRateCharges()
        {
            return _eleRateCharges.GetText();
        }

        public void clickButtonOpenAndSign()
        {
            _btnOpenAndSign.Click();
            waitForLoadingCompleted();
        }

        public void clickButtonOpenAndPrint()
        {
            _btnOpenAndPrint.Click();
            waitForLoadingCompleted(30);
            DriverUtils.WaitForTabOpen(2);
            DriverUtils.WaitForPageLoad();
        }

        public void changetUnit(RepairOrderData data)
        {
            if (_btnChangeUnit.IsDisplayed())
            {
                _btnChangeUnit.Click();
                waitForLoadingCompleted(30);
            }

            _eleUnitRow.Format(data.UnitNumber, 1);
            if (_eleUnitRow.IsDisplayed())
            {
                _eleUnitRow.ScrollToView();
                _eleUnitRow.ClickByJs();
                waitForLoadingCompleted();
            }
        }

        public void checkOpenAgreementWithoutSignature(bool isChecked)
        {
            if (_ckOpenWithoutSignature.IsChecked() == isChecked)
                return;

            _ckOpenWithoutSignature.ClickByJs();
        }

        public string getValueMilesOut()
        {
            _txtMilesOut.WaitForVisible(30);
            return _txtMilesOut.GetValue();
        }
        #endregion
    }
}
