﻿using SeleniumCSharp.Core.DriverWrapper;
using SeleniumCSharp.Core.ElementWrapper;
using SeleniumTests.DataObject;
using SeleniumTests.PageObject.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Agreement
{
    class ViewAgreementPage : BasePage
    {
        #region Elements
        private Label _lblStatus = new Label("id=tsd-agreement-status");
        #endregion

        public ViewAgreementPage()
        {
            RequiredElement = _lblStatus;
        }
        #region Business Methods
        public string getStatus()
        {
            return _lblStatus.GetText();
        }
        #endregion
    }
}
