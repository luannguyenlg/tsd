﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumCSharp.Core.DriverWrapper;
using SeleniumCSharp.Core.ElementWrapper;
using SeleniumCSharp.Core.Helpers;
using SeleniumTests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Global
{
    class BasePage
    {
        #region Properties
        protected BaseElement RequiredElement { get; set; }

        protected BaseElement embed = new BaseElement("//embed");
        protected BaseElement loader = new BaseElement("//div[contains(@class,'loading') and ./parent::*[not(contains(@class,'aurelia-hide'))]]//div[@class='cloud']");
        protected BaseElement toastMessage = new BaseElement("//div[@class='message__text']");

        protected Button _btnCloseDialog = new Button("//button[contains(@class,'dialog-close')]");
        protected Button _btnConfirmDialog = new Button("id=tsd-dialog-action");
        protected Button _btnCancelDialog = new Button("id='tsd-dialog-cancel");
        protected Button _btnSaveDialog = new Button("//ux-dialog//button[text()='Save']");


        protected Link _lnkLogo = new Link("//a[@href='/']");

        protected Label _lblPageTitle = new Label("//h1");
        protected Label _lblPageHeaderTitle = new Label("//h2[@class='tsd-page-header__title']");
        #endregion

        #region Actions
        public void WaitForPageLoad(int timeout = Constants.waitForPageTimeout, bool isCloseDialog = false)
        {
            waitForLoadingCompleted();
            if (RequiredElement != null)
            {
                RequiredElement.WaitForVisible(timeout);
            }
            if(isCloseDialog == true)
                closeAllDialog();
        }

        public bool IsPageDisplayed()
        {
            if (RequiredElement != null)
            {
                return RequiredElement.IsDisplayed();
            }
            else return false;
        }

        public void closeAllDialog()
        {
            try {
                if (_btnCloseDialog.IsDisplayed())
                    foreach (IWebElement ele in _btnCloseDialog.GetElements())
                        ele.Click();
                        /*if (ele.IsDisplayed())
                            ele.ClickByJs();*/
            }
            catch (WebDriverTimeoutException e)
            {
                return;
            }
        }

        public void waitForLoadingCompleted(int visibleTimeOut = 10, int inVisibleTimeOut = 180)
        {
            try
            {
                loader.WaitForVisible(visibleTimeOut);
                if (loader.IsDisplayed())
                    WaitHelper.WaitUntil(() => loader.IsDisplayed() == false, TimeSpan.FromSeconds(inVisibleTimeOut), TimeSpan.FromSeconds(1));

            }
            catch (Exception ex)
            {
                if(ex is WebDriverTimeoutException || ex is StaleElementReferenceException || ex.InnerException is StaleElementReferenceException)
                    return;
                throw;
            }
        }

        public string getTextPageHeaderTitle()
        {
            _lblPageHeaderTitle.WaitForVisible(60);
            return _lblPageHeaderTitle.GetText();
        }

        public void confirmDialog()
        {
            _btnConfirmDialog.ClickByJs();
        }

        public void cancelDialog()
        {
            _btnCancelDialog.ClickByJs();
        }

        public void saveDialog()
        {
            _btnSaveDialog.ClickByJs();
        }

        public void gotoHomePage()
        {
            _lnkLogo.ClickByJs();
            waitForLoadingCompleted();
        }

        public void scrollTop()
        {
            DriverUtils.ExecuteScript("window.scrollTo(0, 0)");
        }

        public string getAttributeEmbed(string attr)
        {
            embed.WaitForVisible(180);
            return embed.GetAttribute(attr);
        }

        public string getToastMessage()
        {
            toastMessage.WaitForVisible();
            return toastMessage.GetText();
        }
        #endregion
    }
}
