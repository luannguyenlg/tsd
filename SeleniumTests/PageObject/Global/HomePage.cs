﻿using SeleniumCSharp.Core.ElementWrapper;
using SeleniumTests.PageObject.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject
{
    class HomePage : BasePage
    {
        #region Elements       
        private Button _btnLogout = new Button("//div[@class='nav-icon-element']//a[@href='/logout']");
        private Button _btnAdd = new Button("id=tsd-floating-button");
        private Button _btnLoaner = new Button("//button[text()='Loaner']");
        private Button _btnNewAppointment = new Button("id=tsd-new-reservation");
        private Button _btnNewCustomer = new Button("id=tsd-new-patron");

        private Link _lnkHubLink = new Link("//div[@class='tsd-hub-links']//a[contains(@href,'{0}')]");
        private BaseElement _eleDialog = new BaseElement("//ux-dialog[.//div//span[contains(text(),'{0}')]]");
        #endregion

        #region Business Methods
        public void logout()
        {
            _btnLogout.Click();
        }

        public HomePage()
        {
            RequiredElement = _btnAdd;
        }

        public void openTab(HubTab tab)
        {
            switch (tab)
            {
                case HubTab.Appointment:
                    _lnkHubLink.Format("reservation");
                    break;  
                case HubTab.Agreement:
                    _lnkHubLink.Format("agreement");
                    break;  
                case HubTab.Customer:
                    _lnkHubLink.Format("patron");
                    break;
            }
            _lnkHubLink.ClickByJs();
        }

        public void addNew(AddType type)
        {
            _btnAdd.ClickByJs();
            switch (type)
            {
                case AddType.Appointment:
                    _btnNewAppointment.WaitForVisible();
                    _btnNewAppointment.ClickByJs();
                    break;
                case AddType.Customer:
                    _btnNewAppointment.WaitForVisible();
                    _btnNewCustomer.ClickByJs();
                    break;
            }
        }

        public bool isDisplayedDialog(string header)
        {
            _eleDialog.Format(header);
            return _eleDialog.IsDisplayed();
        }

        public void clickButtonLoaner()
        {
            _btnLoaner.Click();
        }

        #endregion
    }

    enum AddType
    {   
        Customer,
        Appointment,
    }

    enum HubTab
    {
        SearchOpen,
        Customer,
        Appointment,
        Agreement,
        Fleet
    }
}
