﻿using SeleniumCSharp.Core.ElementWrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Hub
{
    class AgreementHubPage : HomePage
    {
        #region Elements
        private Button _btnSearch = new Button("//button[text()='Search']");
        private Button _btnCloseAgreement = new Button("//article[contains(normalize-space(), '{0}')]//button[normalize-space()='Close']");

        private TextBox _txtSearch = new TextBox("id=navsearchfield");
        #endregion

        public AgreementHubPage()
        {
            RequiredElement = _txtSearch;
        }

        #region Business Methods
        public void search(string text)
        {
            _txtSearch.SendKeys(text);
            _btnSearch.Click();
            waitForLoadingCompleted();
        }

        public void closeAgreement(string content)
        {
            _btnCloseAgreement.Format(content);
            _btnCloseAgreement.WaitForVisible(60);
            _btnCloseAgreement.ScrollToView();
            _btnCloseAgreement.Click();
            waitForLoadingCompleted();
        }

        #endregion
    }

}
