﻿using SeleniumCSharp.Core.ElementWrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Hub
{
    class AppointmentHubPage : HomePage
    {
        #region Elements
        private BaseElement _eleSearchResultList = new BaseElement("//div[@class='tsd-hub-results__hits']");

        private Button _btnSearch = new Button("//button[text()='Search']");
        private Button _btnCreateAgreement = new Button("//article[contains(normalize-space(), '{0}')]//button[normalize-space()='Create Agreement']");
        private Button _btnCancelAgreement = new Button("//article[contains(normalize-space(), '{0}')]//button[normalize-space()='Cancel']");

        private CheckBox _ckStatus = new CheckBox("//label[contains(@class,'tsd-checkbox-element') and contains(normalize-space(),'{0}')]//input");
        private TextBox _txtSearch = new TextBox("id=tsdSearch");
        #endregion

        public AppointmentHubPage()
        {
            RequiredElement = _txtSearch;
        }

        #region Business Methods
        public void search(string text)
        {
            _txtSearch.SendKeys(text);
            _btnSearch.Click();
            waitForLoadingCompleted();
        }

        public void createAgreement(string content)
        {
            _btnCreateAgreement.Format(content);
            _btnCreateAgreement.WaitForVisible(60);
            _btnCreateAgreement.ScrollToView();
            _btnCreateAgreement.Click();
            waitForLoadingCompleted();
        }

        public void cancelAgreement(string content, bool cancelAll = false)
        {
            _btnCancelAgreement.Format(content);
            if(_btnCancelAgreement.IsDisplayed() == true)
            {
                foreach (BaseElement ele in _btnCancelAgreement.GetListElements()) {
                    if (ele.IsDisplayed() == false)
                        return;
                    ele.ScrollToView();
                    ele.ClickByJs();
                    confirmDialog();
                    waitForLoadingCompleted();
                    if (cancelAll == false)
                        break;
                }
            }
        }

        public bool isDisplayedSearchResultList()
        {
            _eleSearchResultList.WaitForVisible(30);
            return _eleSearchResultList.IsDisplayed();
        }

        public void selectStatus(StatusType status)
        {
            switch (status)
            {
                case StatusType.Booked:
                    _ckStatus.Format("Booked");
                    break;
            }
            _ckStatus.ClickByJs();
        }
        #endregion
    }

    enum StatusType
    {
        Booked,
    }

}
