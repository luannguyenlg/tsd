﻿using SeleniumCSharp.Core.ElementWrapper;
using SeleniumTests.DataObject;
using SeleniumTests.PageObject.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Login
{
    class NewLoanerAppointmentPage : BasePage
    {
        #region Elements
        private BaseElement _eleCustomerInfo = new BaseElement("//div[@class='tsd-step-summary']");
        private BaseElement _eleListOfUnits = new BaseElement("//section[@class='tsd-automobile-availability']");
        private BaseElement _eleUnitRow = new BaseElement("//section[@class='tsd-automobile-availability']//tbody//tr[{0}][{1}]");
        private string _sRowContent = ".//td[contains(text(),'{0}')]";
        private string _sRowNotContent = "not(.//td[(contains(normalize-space(),'{0}'))])";

        private Button _btnSave = new Button("//button[text()='Save']");
        private Button _btnContinue = new Button("//button[text()='Continue']");
        private Button _btnSelectUnit = new Button("//button[text()='Select Unit']");

        private TextBox _txtFirstName = new TextBox("//input[@data-qa='firstName']");
        private TextBox _txtLastName = new TextBox("//input[@data-qa='lastName']");
        private TextBox _txtMiddleName = new TextBox("//input[@data-qa='middleName']");
        private TextBox _txtPhone = new TextBox("//input[@data-qa='phone']");
        private TextBox _txtEmail = new TextBox("//input[@data-qa='email']");
        private TextBox _txtAddress1 = new TextBox("//input[@data-qa='address1']");
        private TextBox _txtAddress2 = new TextBox("//input[@data-qa='address2']");
        private TextBox _txtCity = new TextBox("//input[@data-qa='city']");
        private TextBox _txtZIP = new TextBox("//input[@data-qa='postalCode']");
        private TextBox _txtReturnDate = new TextBox("id=endDatePicker");
        private TextBox _txtNotes = new TextBox("//textarea");
       

        private ComboBox _cbCountry = new ComboBox("//ul[@class='tsd-address']//select[@data-qa='countries']");
        private ComboBox _cbServiceAdvisor = new ComboBox("id=service-advisor");
        private ComboBox _cbBusinessSource = new ComboBox("id=repair-order-type");
        private ComboBox _cbRateCode = new ComboBox("id=rate-code-selector");
        private ComboBox _cbModelGroup = new ComboBox("id=model-group-selector");

        private Label _lblCustomerInfoFullName = new Label("//span[@class='tsd-step-summary__title']");
        private Label _lblCustomerInfoPhone = new Label("//div[@data-item='phone']");
        private Label _lblCustomerInfoEmail = new Label("//div[@data-item='email']");
        private Label _lblSelectedUnit = new Label("//div[contains(text(),'Currently Selected Unit:')]");
        #endregion

        public NewLoanerAppointmentPage()
        {
            RequiredElement = _txtFirstName;
        }
        #region Business Methods
        public void fillDriverData(DriverData data)
        {
            _txtFirstName.SendKeys(data.FirstName);
            _txtLastName.SendKeys(data.LastName);
            _txtMiddleName.SendKeys(data.MiddleName);
            _txtPhone.SendKeys(data.Phone);
            _txtEmail.SendKeys(data.Email);
            _txtAddress1.SendKeys(data.AddressLine1);
            _txtAddress2.SendKeys(data.AddressLine2);
            _txtCity.SendKeys(data.City);
            _txtZIP.SendKeys(data.ZIP);
            _cbCountry.SelectByText(data.CountryAddress);
        }

        public void fillRepairOrderData(RepairOrderData data)
        {
            waitForLoadingCompleted(visibleTimeOut:30, inVisibleTimeOut:210);
            _cbServiceAdvisor.WaitForVisible(30);
            _cbServiceAdvisor.WaitForClickable(15);
            _cbServiceAdvisor.SelectByText(data.ServiceAdvisor, true);
            _cbBusinessSource.WaitForVisible(30);
            _cbBusinessSource.WaitForClickable(15);
            _cbBusinessSource.SelectByText(data.BusinessSource, true);
            _txtReturnDate.SendKeys(data.ReturnDate);
            _cbRateCode.WaitForVisible(15);
            _cbModelGroup.WaitForVisible(15);
            _cbRateCode.SetValue(data.RateCode);
            _cbModelGroup.SetValue(data.ModelGroup);
        }

        public void enterNote(string note)
        {
            _txtNotes.SendKeys(note);
        }

        public void clickButtonSave()
        {
            _btnSave.ScrollToView();
            _btnSave.Click();
            waitForLoadingCompleted(visibleTimeOut:30);
            scrollTop();
        }

        public void clickButtonContinue()
        {
            _btnContinue.ScrollToView();
            _btnContinue.ClickByJs();
            waitForLoadingCompleted(visibleTimeOut: 30);
            scrollTop();
        }

        public void clickButtonSelectUnit()
        {
            _btnSelectUnit.ScrollToView();
            _btnSelectUnit.Click();
            waitForLoadingCompleted(30);
        }

        public bool isDisplayedCustomerInfo()
        {
            _eleCustomerInfo.WaitForVisible(60);
            return _eleCustomerInfo.IsDisplayed();
        }

        public bool isDisplayedListOfUnit()
        {
            _eleListOfUnits.WaitForVisible(60);
            return _eleListOfUnits.IsDisplayed();
        }

        public string getTextCustomerInfoName()
        {
            return _lblCustomerInfoFullName.GetText().Trim();
        }

        public string getTextCustomerInfoPhone()
        {
            return _lblCustomerInfoPhone.GetText().Trim();
        }

        public string getTextCustomerInfoEmail()
        {
            return _lblCustomerInfoEmail.GetText().Trim();
        }

        public string getTextSelectedUnit()
        {
            _lblSelectedUnit.WaitForVisible(15);
            return _lblSelectedUnit.GetText().Trim();
        }

        public void selectRate(RepairOrderData repairOrderData)
        {
            _cbRateCode.SelectByText(repairOrderData.RateCode);
        }

        public void selectUnit(string content, string notContains = null ,int row = 1)
        {
            string temp = string.Format(_sRowContent, content);
            if (notContains != null)
                temp = temp + " and " + string.Format(_sRowNotContent, notContains);
            _eleUnitRow.Format(temp, row);
            _eleUnitRow.WaitForVisible(30);
            _eleUnitRow.ScrollToView();
            _eleUnitRow.Click();
        }

        #endregion
    }
}
