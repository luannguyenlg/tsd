﻿using SeleniumCSharp.Core.ElementWrapper;
using SeleniumTests.PageObject.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.LoanerAppointment
{
    class ViewLoanerAppointmentPage : BasePage
    {
        #region Elements
        private Button _btnCancel = new Button("//div[@class='tsd-page-action']//button[contains(normalize-space(),'Cancel')]");
        private Label _lblCustomerInfoTitle = new Label("//patron//h2[@class='tsd-summary-item__title']");
        #endregion

        public ViewLoanerAppointmentPage()
        {
            RequiredElement = _lblCustomerInfoTitle;
        }
        #region Business Methods
        
        public string getTextCustomerInfoName()
        {
            return _lblCustomerInfoTitle.GetText();
        }

        public void cancelAppointment()
        {
            _btnCancel.WaitForVisible();
            _btnCancel.Click();
            confirmDialog();
            waitForLoadingCompleted();
        }
        #endregion
    }
}
