﻿using SeleniumCSharp.Core.ElementWrapper;
using SeleniumTests.DataObject;
using SeleniumTests.PageObject.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Login
{
    class LoginPage : BasePage
    {
        #region Elements
        private Button _btnLogin = new Button("id=tsd_login_btn");
        private TextBox _txtTSDNumber = new TextBox("id=tsd_elmt_cust");
        private TextBox _txtUsername = new TextBox("id=tsd_elmt_user");
        private TextBox _txtPassword = new TextBox("id=tsd_elmt_pw");
        #endregion

        public LoginPage()
        {
            RequiredElement = _btnLogin;
        }
        #region Business Methods
        public void login(string tsdNumber, string username, string password)
        {
            _txtTSDNumber.SendKeys(tsdNumber);
            _txtUsername.SendKeys(username);
            _txtPassword.SendKeys(password);

            _btnLogin.Click();
        }

        public void login(UserData userData)
        {
            this.login(userData.TSDNumber, userData.Username, userData.Password);
        }
        #endregion
    }
}
