﻿using SeleniumCSharp.Core.ElementWrapper;
using SeleniumTests.PageObject.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.PageObject.Login
{
    class Template : BasePage
    {
        #region Elements
        private Button _btnLogin = new Button("id=tsd_login_btn");

        #endregion

        public Template()
        {
            RequiredElement = _btnLogin;
        }
        #region Business Methods
        
        #endregion
    }
}
