﻿using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;
using SeleniumCSharp.Core.Utilities;
using SeleniumTests.DataObject;
using SeleniumTests.PageObject;
using SeleniumTests.PageObject.Agreement;
using SeleniumTests.PageObject.Hub;
using SeleniumTests.PageObject.LoanerAppointment;
using SeleniumTests.PageObject.Login;
using SeleniumTests.Utilities;
using System;

namespace SeleniumTests.Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class TSD_Smoke_Test_01 : TestBase
    {
        [PageObject]
        private readonly LoginPage loginPage = new LoginPage();
        private readonly HomePage homePage = new HomePage();
        private readonly NewLoanerAppointmentPage newLoanerAppointmentPage = new NewLoanerAppointmentPage();
        private readonly ViewLoanerAppointmentPage viewLoanerAppointmentPage = new ViewLoanerAppointmentPage();
        private readonly AppointmentHubPage appointmentHubPage = new AppointmentHubPage();
        private readonly NewAgreementPage newAgreementPage = new NewAgreementPage();


        [DataObject]
        private readonly UserData userData = JsonParser.Get<UserData>();
        private readonly DriverData driverData = JsonParser.Get<DriverData>();
        private readonly RepairOrderData repairOrderData = JsonParser.Get<RepairOrderData>();

        [SetUp]
        public void Precondition()
        {
            driverData.generateNewData();
            repairOrderData.generateNewData();
            driverData.LastName = driverData.LastName;

            Business.Login(userData);
            Business.CancelAllReservationByContent(driverData.LastName);
            homePage.gotoHomePage();
        }

        [TearDown]
        public void Cleaning()
        {
            // it good thing to cleaning up data test through api
            homePage.gotoHomePage();
            Business.CancelAllReservationByContent(driverData.LastName);
        }

        [Test]
        [Category("Smoke")]
        [Category("Reservation")]
        [Description("Create Reservation")]
        public void TestCase()
        {
            Log.Info("1.Click + at bottom right of Search hub ");
            Log.Info("2.Select New Appointment from the menu");
            homePage.addNew(AddType.Appointment);
            homePage.isDisplayedDialog("Select Appointment Type")
                .Should().BeTrue("Select Appointment type dialog is not displayed", Constants.ERR_MSG_NOT_DISPLAY);

            Log.Info("3.Select an Appointment Type from the dialog");
            homePage.clickButtonLoaner();
            newLoanerAppointmentPage.WaitForPageLoad();

            Log.Info("4.Enter values for the Driver");
            newLoanerAppointmentPage.fillDriverData(driverData);

            Log.Info("5.Click the blue Save Continue button");
            newLoanerAppointmentPage.clickButtonContinue();
            newLoanerAppointmentPage.isDisplayedCustomerInfo()
                .Should().BeTrue("Customer information is not saved", Constants.ERR_MSG_NOT_DISPLAY);
            newLoanerAppointmentPage.getTextCustomerInfoName()
                .Should().Be(driverData.getFullName(), "Customer information [full-name] is not saved correctly", Constants.ERR_MSG_NOT_DISPLAY);
            newLoanerAppointmentPage.getTextCustomerInfoPhone()
                .Should().Contain(driverData.Phone, "Customer information [phone] is not saved correctly", Constants.ERR_MSG_NOT_DISPLAY);
            newLoanerAppointmentPage.getTextCustomerInfoEmail()
                .Should().Be(driverData.Email, "Customer information [email] is not saved correctly", Constants.ERR_MSG_NOT_DISPLAY);


            /*Enter values for Service Advisor, Business Source, 
             * Repair Order Number(if available), Customer Tag Number, 
             * select a Checkout date/ time that is not in the past, 
             * select a return date that is greater than the Checkout date.*/
            Log.Info("6.Enter some values ");
            Log.Info("7.Select a rate code and model group if available");
            newLoanerAppointmentPage.fillRepairOrderData(repairOrderData);

            Log.Info("8.Click Select Unit");
            newLoanerAppointmentPage.clickButtonSelectUnit();
            newLoanerAppointmentPage.isDisplayedListOfUnit()
                .Should().BeTrue("List of units that are available for pickup on the checkout date are not shown", Constants.ERR_MSG_NOT_DISPLAY);

            Log.Info("9.Select a unit from the list of available units");
            newLoanerAppointmentPage.selectUnit(content:repairOrderData.UnitNumber);
            newLoanerAppointmentPage.getTextSelectedUnit()
                .Should().Contain(repairOrderData.UnitNumber, "unit selected doesn't show next to Currently Selected Unit", Constants.ERR_MSG_NOT_DISPLAY);

            Log.Info("10.Enter in Appointment notes and click the blue save button");
            newLoanerAppointmentPage.enterNote(repairOrderData.Notes);
            newLoanerAppointmentPage.clickButtonSave();

            viewLoanerAppointmentPage.getTextPageHeaderTitle()
                .Should().Contain("APPOINTMENT NUMBER", "Appointment is not saved for the customer", Constants.ERR_MSG_NOT_DISPLAY);
            viewLoanerAppointmentPage.getTextCustomerInfoName()
                .Should().Contain(driverData.getFullName(), "Information entered is saved to the appointment have some errors.", Constants.ERR_MSG_NOT_DISPLAY);

        }
    }
}