﻿using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;
using SeleniumCSharp.Core.DriverWrapper;
using SeleniumCSharp.Core.Utilities;
using SeleniumTests.DataObject;
using SeleniumTests.Helpers;
using SeleniumTests.PageObject;
using SeleniumTests.PageObject.Agreement;
using SeleniumTests.PageObject.Hub;
using SeleniumTests.PageObject.LoanerAppointment;
using SeleniumTests.PageObject.Login;
using SeleniumTests.Utilities;
using System;
using System.Collections.ObjectModel;

namespace SeleniumTests.Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class TSD_Smoke_Test_02 : TestBase
    {
        [PageObject]
/*        private TrelloLoginPage trelloLoginPageObject;*/
        private readonly LoginPage loginPage = new LoginPage();
        private readonly HomePage homePage = new HomePage();
        private readonly NewLoanerAppointmentPage newLoanerAppointmentPage = new NewLoanerAppointmentPage();
        private readonly ViewLoanerAppointmentPage viewLoanerAppointmentPage = new ViewLoanerAppointmentPage();
        private readonly AppointmentHubPage appointmentHubPage = new AppointmentHubPage();
        private readonly NewAgreementPage newAgreementPage = new NewAgreementPage();


        [DataObject]
        private readonly UserData userData = JsonParser.Get<UserData>();
        private readonly DriverData driverData = JsonParser.Get<DriverData>();
        private readonly DriverData newDriverData = JsonParser.Get<DriverData>();
        private readonly RepairOrderData repairOrderData = JsonParser.Get<RepairOrderData>();
        private readonly AutomobileData automobileData = JsonParser.Get<AutomobileData>();


        [SetUp]
        public void Precondition()
        {
            driverData.generateNewData();
            repairOrderData.generateNewData("");

            Business.Login(userData);
            Business.CancelAllReservationByContent(driverData.LastName);
            Business.CreateReservation(driverData, repairOrderData);
            homePage.gotoHomePage();
        }

        [TearDown]
        public void Cleaning()
        {
            // it good thing to cleaning up data test through api
            Console.WriteLine("cleaning up...");
        }

        [Test]
        [Category("Smoke")]
        [Category("Appointment to Agreement")]
        [Description("Open Appointment to Agreement")]
        public void TestCase()
        {
            //Prepare data for testing
            string today = DateHelper.GetCurrentDateString(dateFormat:"M/dd/yyyy");
            string fee = "CDW";
            string agreementNotes = "AGREEMENT NOTE " + DateHelper.GetCurrentDateString();
            string beforeUpdate = "";
            string pdfType = "application/pdf";


            Log.Info("1.click the appointment tab ");
            homePage.openTab(HubTab.Appointment);

            Log.Info("2.Search for the Appointment");
            appointmentHubPage.search(driverData.FirstName);
            appointmentHubPage.isDisplayedSearchResultList()
                .Should().BeTrue("Appointment is not shown in list form", Constants.ERR_MSG_NOT_DISPLAY);

            Log.Info("3.Click the Create Agreement button");
            appointmentHubPage.createAgreement(driverData.FirstName);
            newAgreementPage.WaitForPageLoad();
            newAgreementPage.IsPageDisplayed()
                .Should().BeTrue("New Agreement page is not shown", Constants.ERR_MSG_NOT_DISPLAY);

            Log.Info("4.Edit Customer Information");
            driverData.generateNewData();
            newAgreementPage.fillAgreement(driverData);

            Log.Info("5.Edit Inurance Information");
            newAgreementPage.fillInsurance(driverData);

            Log.Info("6.Click Add Driver");
            Log.Info("7.Add Driver");
            newDriverData.generateNewData();
            newAgreementPage.addDriver(newDriverData);

            Log.Info("8.Click Add Customer Automobile");
            Log.Info("9.Add Customer Automobile");
            newAgreementPage.addAutomobile(automobileData);

            Log.Info("10.Edit Repair Order Information");
            repairOrderData.BusinessSource = "Service RO";
            newAgreementPage.fillRepairOrder(repairOrderData);

            Log.Info("11.Review Checkout Date/time");
            newAgreementPage.getValueCheckoutDate()
                .Should().Be(today, "Check Out date not set to now", Constants.ERR_MSG_NOT_DISPLAY);

            Log.Info("12.Change Rate Information");
            repairOrderData.RateCode = "Summer Deal 👁";
            repairOrderData.ModelGroup = "Premium";
            newAgreementPage.fillRate(repairOrderData);

            Log.Info("13.Add Fees");
            newAgreementPage.addFee(fee);

            Log.Info("14.Edit Unit");
            repairOrderData.UnitNumber = "";
            newAgreementPage.changetUnit(repairOrderData);

            Log.Info("14.Change Miles  to be larger than what is shown");
            beforeUpdate = newAgreementPage.getValueMilesOut();
            newAgreementPage.changeMilesOut((int.Parse(beforeUpdate) + 1).ToString());

            Log.Info("15.Add Agreement notes");
            newAgreementPage.changeAgreementNotes(agreementNotes);

            Log.Info("16.Click Open and Print");
            newAgreementPage.checkOpenAgreementWithoutSignature(true);
            newAgreementPage.clickButtonOpenAndPrint();
            DriverUtils.SwitchTab(1);
            homePage.getAttributeEmbed("type")
                .Should().Be(pdfType, "Agreement is not shown in pdf form in a new tab for  printing", Constants.ERR_MSG_NOT_DISPLAY);
        }
    }
}