﻿using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;
using SeleniumCSharp.Core.DriverWrapper;
using SeleniumCSharp.Core.Utilities;
using SeleniumTests.DataObject;
using SeleniumTests.Helpers;
using SeleniumTests.PageObject;
using SeleniumTests.PageObject.Agreement;
using SeleniumTests.PageObject.Hub;
using SeleniumTests.PageObject.LoanerAppointment;
using SeleniumTests.PageObject.Login;
using SeleniumTests.Utilities;
using System;
using System.Collections.ObjectModel;

namespace SeleniumTests.Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class TSD_Smoke_Test_03 : TestBase
    {
        [PageObject]
/*        private TrelloLoginPage trelloLoginPageObject;*/
        private readonly HomePage homePage = new HomePage();
        private readonly AgreementHubPage agreementHubPage = new AgreementHubPage();
        private readonly CloseAgreementPage closeAgreementPage = new CloseAgreementPage();
        private readonly ViewAgreementPage viewAgreementPage = new ViewAgreementPage();

        [DataObject]
        private readonly UserData userData = JsonParser.Get<UserData>();
        private readonly DriverData driverData = JsonParser.Get<DriverData>();
        private readonly RepairOrderData repairOrderData = JsonParser.Get<RepairOrderData>();

        [SetUp]
        public void Precondition()
        {
            driverData.generateNewData();
            repairOrderData.generateNewData("");

            Business.Login(userData);
            Business.CancelAllReservationByContent(driverData.LastName);
            Business.CreateReservation(driverData, repairOrderData);
            Business.CreateAgreement(driverData, repairOrderData);
            homePage.gotoHomePage();
        }

        [TearDown]
        public void Cleaning()
        {
            Console.WriteLine("cleaning up...");
        }

        [Test]
        [Category("Smoke")]
        [Category("Agreement")]
        [Description("Close Agreement")]
        public void TestCase()
        {
            //Prepare data for testing
            string today = DateHelper.GetCurrentDateString(dateFormat:"MM/dd/yyyy");
            string beforeUpdate = "";

            Log.Info("1.On the operations page click on the Agreements hubcard labeled Open");
            homePage.openTab(HubTab.Agreement);
            agreementHubPage.WaitForPageLoad();

            Log.Info("2.Click the Close button to the right of the agreement you wish to close");
            agreementHubPage.search(driverData.FirstName);
            agreementHubPage.closeAgreement(driverData.FirstName);

            Log.Info("3.Modify the return date/time");
            closeAgreementPage.getValueReturnDate()
                .Should().Be(today, "Return date not set to now", Constants.ERR_MSG_NOT_DISPLAY);

            Log.Info("4.Enter a valid  integer value for Miles");
            Log.Info("5.Select a fuel level in");
            beforeUpdate = closeAgreementPage.getValueMilesOut();
            closeAgreementPage.enterMilesIn((int.Parse(beforeUpdate) + 1).ToString());

            Log.Info("6.Click Close button");
            closeAgreementPage.clickButtonClose();
            viewAgreementPage.getToastMessage()
                .Should().Contain("Agreement sent successfully to", "Email didn't send to renter", Constants.ERR_MSG_NOT_DISPLAY);
            viewAgreementPage.getStatus()
                .Should().Be("Close", "Agreement is not closed", Constants.ERR_MSG_NOT_DISPLAY);
        }
    }
}