﻿using System;
using NUnit.Framework;
using SeleniumCSharp.Core.DriverWrapper;
using SeleniumCSharp.Core.Utilities;
using SeleniumTests.DataObject;
using SeleniumTests.PageObject;
using SeleniumTests.PageObject.Agreement;
using SeleniumTests.PageObject.Hub;
using SeleniumTests.PageObject.LoanerAppointment;
using SeleniumTests.PageObject.Login;

namespace SeleniumTests.Utilities
{
    public class Business
    {
        private static LoginPage loginPage = new LoginPage();
        private static HomePage homePage = new HomePage();
        private static NewLoanerAppointmentPage newLoanerAppointmentPage = new NewLoanerAppointmentPage();
        private static ViewLoanerAppointmentPage viewLoanerAppointmentPage = new ViewLoanerAppointmentPage();
        private static AppointmentHubPage appointmentHubPage = new AppointmentHubPage();
        private static NewAgreementPage newAgreementPage = new NewAgreementPage();

        public static void Login(UserData userData)
        {
            loginPage.WaitForPageLoad();
            loginPage.login(userData);
            homePage.WaitForPageLoad(isCloseDialog: true);
        }

        public static void CreateReservation(DriverData driverData, RepairOrderData repairOrderData)
        {
            homePage.gotoHomePage();
            homePage.addNew(AddType.Appointment);
            homePage.clickButtonLoaner();
            newLoanerAppointmentPage.WaitForPageLoad();
            newLoanerAppointmentPage.fillDriverData(driverData);
            newLoanerAppointmentPage.clickButtonContinue();
            newLoanerAppointmentPage.fillRepairOrderData(repairOrderData);
            newLoanerAppointmentPage.enterNote(repairOrderData.Notes);
            newLoanerAppointmentPage.clickButtonSelectUnit();
            newLoanerAppointmentPage.selectUnit(repairOrderData.UnitNumber, notContains: "MOTORQ5");
            repairOrderData.UnitNumber = newLoanerAppointmentPage.getTextSelectedUnit()
                .Split(new string[] { ": " }, StringSplitOptions.None)[1];
            newLoanerAppointmentPage.clickButtonSave();
        }

        public static void CancelAllReservationByContent(String content)
        {
            homePage.gotoHomePage();
            homePage.openTab(HubTab.Appointment);
            appointmentHubPage.search(content);
            appointmentHubPage.selectStatus(StatusType.Booked);
            appointmentHubPage.cancelAgreement(content, true);
        }

        public static void CreateAgreement(DriverData driverData, RepairOrderData repairOrderData)
        {
            homePage.gotoHomePage();
            homePage.openTab(HubTab.Appointment);
            appointmentHubPage.search(driverData.FirstName);
            appointmentHubPage.createAgreement(driverData.FirstName);
            newAgreementPage.WaitForPageLoad();
            newAgreementPage.fillAgreement(driverData);
            newAgreementPage.fillInsurance(driverData);
            repairOrderData.UnitNumber = "";
            newAgreementPage.changetUnit(repairOrderData);
            newAgreementPage.checkOpenAgreementWithoutSignature(true);
            newAgreementPage.clickButtonOpenAndPrint();
            DriverUtils.SwitchTab(1);
            DriverUtils.CloseCurrentTab();
        }
    }
}