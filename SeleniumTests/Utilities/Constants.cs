﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests.Utilities
{
    public class Constants
    {
        public const int LogRetentionDays = 1;
        public const string LogExtension = ".log";
        public const string ImgExtension = ".png";

        public const string ERR_MSG_NOT_DISPLAY = "Error message doesn't display";
        public const int waitForElementTimeout = 60;
        public const int waitForPageTimeout = 120;
    }
}
